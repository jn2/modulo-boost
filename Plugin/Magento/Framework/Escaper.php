<?php

declare(strict_types=1);

namespace Jn2\Boostcommerce\Plugin\Magento\Framework;

use Magento\Framework\Escaper as Subject;

/**
 * Class Escaper
 * @package Jn2\Boostcommerce\Plugin\Magento\Framework
 */
class Escaper
{
    /**
     * Plugin para corrigir a issue 33346 que ocorre na função que monta URLs
     * só aceitar parâmetros como string.
     *
     * @todo Remover esse plugin quando o bug for corrigido no core
     * @see https://github.com/magento/magento2/issues/33346
     *
     * @param Subject $subject
     * @param $string
     * @return array
     */
    public function beforeEncodeUrlParam(Subject $subject, $string): array
    {
        return [(string)$string];
    }
}
