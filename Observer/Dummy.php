<?php

declare(strict_types=1);

namespace Jn2\Boostcommerce\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Dummy
 * @package Jn2\Boostcommerce\Observer
 */
class Dummy implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {

    }
}
